class Node {
    constructor(data) {
        this.data = data
        this.next = null
    }
}

class LinkedList {
    constructor() {
        this.head = this.tail = null
    }
    appendNode(data) {
        let _node = new Node(data)
        if (this.head) {
            this.tail.next = _node
            this.tail = _node
            console.log(`New node data = ${data} appended!`)
        } else {
            this.head = this.tail = _node;
            console.log(`New node data = ${data} appended!`)
        }
    }

    insertNode(index, data) {
        let _next = this.search(index)
        let _node = new Node(data)
        this.search(index - 1).next = _node;
        // console.log('** L26 index-1.next = ', this.search(index - 1).next)
        _node.next = _next
        console.log(`New node data = ${data} inserted!`)
    }

    prependNode(data) {
        let _node = new Node(data)
        if (this.head) {
            _node.next = this.head
            this.head = _node
            console.log(`New node data = ${data} prepended!`)
        } else {
            this.head = this.tail = _node
            console.log(`New node data = ${data} prepended!`)
        }
    }

    showList() {
        console.log('\n\n***** Show Linked List ***** \n\nhead => ', this.head.data)
        let current = this.head
        while (current.next) {
            if (current !== this.head && current.next)
                console.log(current.data)
            current = current.next
        }
        console.log('tail => ', current.data, '\n\n*****************************\n\n')
    }

    search(index) {
        let current = this.head
        let count = 0
        while (current.next) {
            if (count === index) {
                return current;
            } else {
                current = current.next
                count++
            }
        }
        return current
    }

    size() {
        let _count = 1;
        if (this.head) {
            // let _count = 1;
            let current = this.head
            while (current.next) {
                current = current.next
                _count++
            }
        } else {
            _count = 0
        }
        return _count;
    }

    delete(index) {
        let current = this.head
        let _count = 0

        // delete the first node of the list
        if (!!!index) {
            this.head = current.next
            console.log(`Node with index = ${index} removed!`)
        }

        // delete the last node in the list
        else if (this.size() === index) {
            let to_be_deleted = this.search(index)
            this.tail = this.search(index - 1)
            this.tail.next = null

            console.log(`Node with index = ${index} removed!`)
                // Collect garbage
            to_be_deleted.data = null
            to_be_deleted.next = null
            return
        }
        // delete a node in the middle of the Linked List
        else {
            while (current.next) {
                if (_count !== index) {
                    current = current.next
                    _count++
                } else {
                    this.search(index - 1).next = current.next
                    console.log(`Node with index = ${index} removed!`)
                        // collect garbage
                    current.data = null;
                    current.next = null
                    return
                }
            }
        }
    }
}

let ll = new LinkedList()
console.log('new size = ', ll.size())
ll.appendNode('tuesday')
console.log('new size = ', ll.size())
ll.appendNode(5)
ll.appendNode(7)
ll.appendNode(8)
ll.prependNode('monday')
ll.showList()
ll.delete(3)
ll.showList()
ll.delete(2)
ll.showList()
ll.delete(1)
ll.showList()
console.log('new size = ', ll.size())
ll.insertNode(1, 'wednesday')
ll.showList()
console.log('new size = ', ll.size())